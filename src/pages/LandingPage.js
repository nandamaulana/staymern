import React, { Component } from "react";
import { connect } from "react-redux";
import BlockUi from "react-block-ui";
import { Loader } from 'react-loaders';
import 'react-block-ui/style.css';
import 'loaders.css/loaders.min.css';

import Header from "parts/Header";
import Hero from "parts/Hero";
import MostPicked from "parts/MostPicked";
import Categories from "parts/Categories";

import { fetchPage } from "store/actions/page";
import Testimony from "parts/Testimony";
import Footer from "parts/Footer";

class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.refMostPicked = React.createRef();
  }

  componentDidMount() {
    window.title = "Details Page";
    window.scrollTo(0, 0);

    if (!this.props.page.landingPage) {
      this.props.fetchPage(
        `/landing-page`,
        "landingPage"
      );
    }
  }

  render() {
    const { page } = this.props;
    if (!page.hasOwnProperty("landingPage"))
      return (
        <div className="container">
          <div
            className="row align-items-center justify-content-center text-center"
            style={{ height: "100vh"}}
          >
            <BlockUi tag="div" blocking={true} loader={<Loader active type={`line-scale-pulse-out-rapid`} color="#3353DF"/>} ><span className="text-white">Loading</span></BlockUi>
          </div>
        </div>
      );

    return (
      <>
        <Header {...this.props} />
        <Hero refMostPicked={this.refMostPicked} data={page.landingPage.hero} />
        <MostPicked
          refMostPicked={this.refMostPicked}
          data={page.landingPage.mostPicked}
        />
        <Categories data={page.landingPage.category} />
        <Testimony data={page.landingPage.testimonial} />
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  page: state.page,
});

export default connect(mapStateToProps, { fetchPage })(LandingPage);
