import React from "react";

import Star from "elements/Star";

import testimonyFrame from "assets/images/testimonial-frame.jpg";
import Button from "elements/Button";

import Fade from "react-reveal/Fade";

export default function Testimony({ data }) {
  return (
    <section className="container">
      <div className="row align-items-center">
        <Fade left>
          <div className="col-auto" style={{ marginRight: 70 }}>
            <div
              className="testimonial-hero"
              style={{ margin: `30px 0 0 30px` }}
            >
              <img
                src={`${process.env.REACT_APP_HOST}/${data.imageUrl}`}
                alt="Testimonial"
                className="position-absolute"
                style={{ zIndex: 1 }}
              />
              <img
                src={testimonyFrame}
                alt="Testimonial frame"
                className="position-absolute"
                style={{ margin: `-30px 0 0 -30px` }}
              />
            </div>
          </div>
        </Fade>
        <Fade right delay={300}>
          <div className="col" style={{ marginBottom: 40 }}>
            <h4>{data.name}</h4>
            <Star value={data.rate} width={35} height={35} spacing={3} />
            <h5 className="h2 font-weight-light line-height-2 my-3">
              {data.content}
            </h5>
            <span className="text-gray-500">
              {data.familyName}, {data.familyOccupation}
            </span>
            <div>
              <Button
                type="link"
                className="btn px-5"
                style={{ marginTop: 40 }}
                hasShadow
                isPrimary
                href={`/testimonial/${data._id}`}
              >
                Read Their Story
              </Button>
            </div>
          </div>
        </Fade>
      </div>
    </section>
  );
}
